#include <stdio.h>
#include <stdlib.h>

#ifdef __APPLE__
#include <OpenCL/opencl.h>
#else
#include <CL/cl.h>
#endif

#define MAX_SOURCE_SIZE (0x100000)

void write(int* R, int n)
{
	int i, j;
	for (i = 0; i < n * n; i++)
	{
		printf("%d ", R[i]);
		if (i % n == n - 1 && i != 0)
			printf("\n");
	}
	printf("\n");
}

int main(void) {
	// Reading and allocating memory for matrix
	int i, j;
	int dim_n;

	FILE* fmatrix;
	fopen_s(&fmatrix, "inputA.txt", "r");

	fscanf_s(fmatrix, "%d", &dim_n);

	int* A = (int*)malloc(sizeof(int) * dim_n * dim_n + 4);
	int* B = (int*)malloc(sizeof(int) * dim_n * dim_n + 4);
	int* Result = (int*)malloc(sizeof(int) * dim_n * dim_n + 4);

	for (i = 0; i < dim_n * dim_n; i++)
		fscanf_s(fmatrix, "%d", &A[i]);
	for (i = 0; i < dim_n * dim_n; i++)
		fscanf_s(fmatrix, "%d", &B[i]);
	fclose(fmatrix);

	write(A, dim_n);
	write(B, dim_n);

	// Load the kernel source code into the array source_str
	FILE* fp;
	char* source_str;
	size_t source_size;

	fopen_s(&fp, "vector_add_kernel.cl", "r");
	if (!fp) {
		fprintf_s(stderr, "Failed to load kernel.\n");
		exit(1);
	}
	source_str = (char*)malloc(MAX_SOURCE_SIZE);
	source_size = fread(source_str, 1, MAX_SOURCE_SIZE, fp);
	fclose(fp);

	// Get platform and device information
	cl_platform_id platform_id = NULL;
	cl_device_id device_id = NULL;
	cl_uint ret_num_devices;
	cl_uint ret_num_platforms;

	//Get platform and device info
	cl_int ret = clGetPlatformIDs(1, &platform_id, &ret_num_platforms);//get the number of platforms
	ret = clGetDeviceIDs(platform_id, CL_DEVICE_TYPE_DEFAULT, 1,
		&device_id, &ret_num_devices); //get the platform id

	// Create an OpenCL context
	cl_context context = clCreateContext(NULL, 1, &device_id, NULL, NULL, &ret);

	// Create a command queue
	cl_command_queue command_queue = clCreateCommandQueueWithProperties(context, device_id, 0, &ret);

	// Create memory buffers on the device for each vector 
	cl_mem a_mem_obj = clCreateBuffer(context, CL_MEM_READ_ONLY, dim_n * sizeof(int) * dim_n, NULL, &ret);
	cl_mem b_mem_obj = clCreateBuffer(context, CL_MEM_READ_ONLY, dim_n * sizeof(int) * dim_n, NULL, &ret);
	cl_mem c_mem_obj = clCreateBuffer(context, CL_MEM_READ_ONLY, dim_n * sizeof(int) * dim_n, NULL, &ret);

	// Copy the lists A and B to their respective memory buffers
	ret = clEnqueueWriteBuffer(command_queue, a_mem_obj, CL_TRUE, 0, dim_n * sizeof(int) * dim_n, A, 0, NULL, NULL);
	ret = clEnqueueWriteBuffer(command_queue, b_mem_obj, CL_TRUE, 0, dim_n * sizeof(int) * dim_n, B, 0, NULL, NULL);
	
	// Create a program from the kernel source
	cl_program program = clCreateProgramWithSource(context, 1, (const char**)&source_str, (const size_t*)&source_size, &ret);

	// Build the program
	ret = clBuildProgram(program, 1, &device_id, NULL, NULL, NULL);

	// Create the OpenCL kernel
	cl_kernel kernel = clCreateKernel(program, "matrix_multiplication", &ret);

	// Set the arguments of the kernel
	ret = clSetKernelArg(kernel, 0, sizeof(cl_mem), (void*)&a_mem_obj);
	ret = clSetKernelArg(kernel, 1, sizeof(cl_mem), (void*)&b_mem_obj);
	ret = clSetKernelArg(kernel, 2, sizeof(cl_mem), (void*)&c_mem_obj);

	// Execute the OpenCL kernel on the list
	size_t global_item_size[2] = { dim_n,dim_n }; // Process the entire lists
	ret = clEnqueueNDRangeKernel(command_queue, kernel, 2, NULL,
		global_item_size, NULL, 0, NULL, NULL);

	if (ret != CL_SUCCESS)
	{
		printf("Error\n");
		exit(1);
	}

	// Read the memory buffer C on the device to the local variable C
	ret = clEnqueueReadBuffer(command_queue, c_mem_obj, CL_TRUE, 0, dim_n * sizeof(int) * dim_n,(void*) Result, 0, NULL, NULL);


	// Display the result to the screen
	printf("\nA*B=\n");
	write(Result, dim_n);

	// Clean up
	ret = clFlush(command_queue);
	ret = clFinish(command_queue);
	ret = clReleaseKernel(kernel);
	ret = clReleaseProgram(program);
	ret = clReleaseMemObject(a_mem_obj);
	ret = clReleaseMemObject(b_mem_obj);
	ret = clReleaseMemObject(c_mem_obj);
	ret = clReleaseCommandQueue(command_queue);
	ret = clReleaseContext(context);
	free(source_str);
	free(A);
	free(B);
	free(Result);

	return 0;
}