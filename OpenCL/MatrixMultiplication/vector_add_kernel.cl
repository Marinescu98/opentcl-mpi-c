__kernel void matrix_multiplication(__global int *A, __global int *B, __global int *C) {
    
    // Get the index of the current element
    int i = get_global_id(1);
    int j = get_global_id(0);
    int dim_n=get_global_size(0);
    int value=0;

    // Do the operation
    for( int k=0;k<dim_n;k++)
    {
        value+=A[k+i*dim_n]*B[k*dim_n+j];
    }
    C[i*dim_n+j]=value;
}
