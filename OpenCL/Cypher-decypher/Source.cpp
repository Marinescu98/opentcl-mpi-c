#include <stdio.h>
#include <stdlib.h>
#include <string>
#include<fstream>

#ifdef __APPLE__
#include <OpenCL/opencl.h>
#else
#include <CL/cl.h>
#endif
#include <iostream>

using namespace std;

#define MAX_SOURCE_SIZE (0x100000)
#define MAXSIZE 100

ifstream fin_cipher("Input_Criptare.txt");
ifstream fin_dec("Input_Decriptare.txt");

enum Status {
	Encrypted,
	Decrypted
};

void generateKey(char* str, char* keyword)
{
	int x = strlen(keyword);
	int dim = x;
	for (int i = 0; i < strlen(keyword); i++)
	{
		if (x == i)
			i = 0;

		if (dim == strlen(str))
			break;

		keyword[dim++] = keyword[i];
	}
	keyword[dim] = '\0';
}



int main(void) {
	// Reading and allocating memory for texts
	int dim;
	//Status status = Decrypted;
	Status status = Encrypted;

	if (status == Encrypted) {
		fin_cipher >> dim;
	}
	else
	{
		fin_dec >> dim;
	}

	dim++;

	char* readText = new char[dim];
	char* encryptedText = new char[dim];
	char* decryptedText = new char[dim];
	char* key = new char[dim];

	if (status == Encrypted)
	{
		fin_cipher >> readText;
		fin_cipher >> key;
	}
	else {
		fin_dec >> readText;
		fin_dec >> key;
	}

	if (strlen(key) > strlen(readText)) {
		printf("Incorrect data.\n");
		exit(1);
	}

	generateKey(readText, key);

	cout << "Text: " << readText << endl;
	cout << "Key: " << key << endl;

	fin_cipher.close();
	fin_dec.close();

	// Load the kernel source code into the array source_str
	FILE* fp;
	char* source_str;
	size_t source_size;

	if (status == Encrypted) {
		fopen_s(&fp, "encryption.cl", "r");
	}
	else
	{
		fopen_s(&fp, "decryption.cl", "r");
	}

	if (!fp) {
		fprintf_s(stderr, "Failed to load kernel.\n");
		exit(1);
	}

	source_str = (char*)malloc(MAX_SOURCE_SIZE);
	source_size = fread(source_str, 1, MAX_SOURCE_SIZE, fp);
	fclose(fp);

	// Get platform and device information
	cl_platform_id platform_id = NULL;
	cl_device_id device_id = NULL;
	cl_uint ret_num_devices;
	cl_uint ret_num_platforms;

	//Get platform and device info
	cl_int ret = clGetPlatformIDs(1, &platform_id, &ret_num_platforms);//get the number of platforms
	ret = clGetDeviceIDs(platform_id, CL_DEVICE_TYPE_DEFAULT, 1,
		&device_id, &ret_num_devices); //get the platform id

	// Create an OpenCL context
	cl_context context = clCreateContext(NULL, 1, &device_id, NULL, NULL, &ret);

	// Create a command queue
	cl_command_queue command_queue = clCreateCommandQueueWithProperties(context, device_id, 0, &ret);

	// Create memory buffers on the device for each text 
	cl_mem text_mem_obj = clCreateBuffer(context, CL_MEM_READ_ONLY, dim * sizeof(char*), NULL, &ret);
	cl_mem key_mem_obj = clCreateBuffer(context, CL_MEM_READ_ONLY, dim * sizeof(char*), NULL, &ret);
	cl_mem buffer_mem_obj = clCreateBuffer(context, CL_MEM_READ_ONLY, dim * sizeof(char*), NULL, &ret);

	// Copy the text to their respective memory buffers
	ret = clEnqueueWriteBuffer(command_queue, text_mem_obj, CL_TRUE, 0, dim * sizeof(char*), readText, 0, NULL, NULL);
	ret = clEnqueueWriteBuffer(command_queue, key_mem_obj, CL_TRUE, 0, dim * sizeof(char*), key, 0, NULL, NULL);

	// Create a program from the kernel source
	cl_program program = clCreateProgramWithSource(context, 1, (const char**)&source_str, (const size_t*)&source_size, &ret);

	// Build the program
	ret = clBuildProgram(program, 1, &device_id, NULL, NULL, NULL);

	// Create the OpenCL kernel
	cl_kernel kernel;
	if (status == Encrypted) {
		kernel = clCreateKernel(program, "encrypting_kernel", &ret);
	}
	else {
		kernel = clCreateKernel(program, "decrypting_kernel", &ret);
	}

	// Set the arguments of the kernel
	ret = clSetKernelArg(kernel, 0, sizeof(cl_mem), (void*)&text_mem_obj);
	ret = clSetKernelArg(kernel, 1, sizeof(cl_mem), (void*)&key_mem_obj);
	ret = clSetKernelArg(kernel, 2, sizeof(cl_mem), (void*)&buffer_mem_obj);

	// Execute the OpenCL kernel on the list
	size_t global_item_size = dim; // Process the entire lists
	ret = clEnqueueNDRangeKernel(command_queue, kernel, 1, NULL,
		&global_item_size, NULL, 0, NULL, NULL);

	if (ret != CL_SUCCESS)
	{
		printf("Error\n");
		exit(1);
	}

	// Read the memory buffer C on the device to the local variable C
	if (status == Encrypted) {
		ret = clEnqueueReadBuffer(command_queue, buffer_mem_obj, CL_TRUE, 0, dim * sizeof(char), (void*)encryptedText, 0, NULL, NULL);
	}
	else {
		ret = clEnqueueReadBuffer(command_queue, buffer_mem_obj, CL_TRUE, 0, dim * sizeof(char), (void*)decryptedText, 0, NULL, NULL);
	}

	// Display the result to the screen
	if (status == Encrypted) {
		encryptedText[strlen(readText)] = '\0';
		cout << "Encrypted text: " << encryptedText << " ";
	}
	else {
		decryptedText[strlen(readText)] = '\0';
		cout << "Decrypted text(original text): " << decryptedText << " ";
	}

	// Clean up
	ret = clFlush(command_queue);
	ret = clFinish(command_queue);
	ret = clReleaseKernel(kernel);
	ret = clReleaseProgram(program);
	ret = clReleaseMemObject(text_mem_obj);
	ret = clReleaseMemObject(key_mem_obj);
	ret = clReleaseMemObject(buffer_mem_obj);
	ret = clReleaseCommandQueue(command_queue);
	ret = clReleaseContext(context);
	free(source_str);
	delete[] readText;
	delete[] encryptedText;
	delete[] decryptedText;
	delete[] key;

	return 0;
}