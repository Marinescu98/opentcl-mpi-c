__kernel void decrypting_kernel(__global char *readText, __global char *key, __global char *decryptedText) {
    
int i = get_global_id(0);

char x = (readText[i] - key[i]+26) % 26;

x += 'A';

decryptedText[i] = x;

}
