__kernel void encrypting_kernel(__global char *readText, __global char *key, __global char *encryptedText) {
    
int i = get_global_id(0);

char x = (readText[i] + key[i]) % 26;

x += 'A';

encryptedText[i] = x;

}
