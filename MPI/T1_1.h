#pragma once

#include "mpi.h"
#include <math.h>
#include <stdio.h>
#include <stdlib.h>

void search_prime(int vect[]);
void prime(int vector[], int all[], int process_id, int n, int number_processes);
void Task1_1();
void print(int v[]);