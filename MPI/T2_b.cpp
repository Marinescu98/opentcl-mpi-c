#include"T2_b.h"


void generate_array2b(int vect[], int n)
{
	srand(9);

	for (int i = 0; i < n; i++)
	{
		vect[i] = rand() % 1000;
	}
}



void print_B(int vect[], int n)
{
	printf("\nArray: ");
	for (int i = 0; i < n; i++)
	{
		printf("v[%d]=%d\n ", i, vect[i]);
	}
	printf("\n");

}



void T2_b() {

	int myid, numprocs;
	int data[MAXSIZE];
	int buffer[MAXSIZE];
	int myresult[MAXSIZE];
	int found = 888;

	MPI_Init(NULL, NULL);

	MPI_Comm_size(MPI_COMM_WORLD, &numprocs);
	MPI_Comm_rank(MPI_COMM_WORLD, &myid);

	/* Check that we have an even number of processes and at most MAXPROC */

	if (numprocs > MAXPROC || numprocs % 2 != 0)
	{
		if (myid == 0)
		{
			printf("You have to use an even number of processes (at most %d)\n", MAXPROC);
		}

		MPI_Finalize();

		exit(0);
	}

	if (0 == myid)
	{
		generate_array2b(data, MAXSIZE);
		print_B(data, MAXSIZE);
		printf("Number search: %d\n\n", found);
	}

	MPI_Scatter(data, MAXSIZE / numprocs, MPI_INT,
		buffer, MAXSIZE / numprocs, MPI_INT,
		0, MPI_COMM_WORLD);
	

	for (int i = 0; i < MAXSIZE / numprocs; i++)
	{
		if (buffer[i] == found)
		{
			buffer[i] = myid * (MAXSIZE / numprocs) + i;
		}
		else
		{
			buffer[i] = -1;
		}
	}
	//printf("Process %d\n", length_all);

	MPI_Gather(buffer, MAXSIZE / numprocs,MPI_INT, myresult, MAXSIZE / numprocs, MPI_INT,0, MPI_COMM_WORLD);

	if (myid == 0)
	{
		printf("\nPositions: ");

		for (int i = 0; i < MAXSIZE; i++)
		{
			if(myresult[i]!=-1)
				printf("***%d ", myresult[i]);
		}
		printf("\n");
	}

	MPI_Finalize();
}