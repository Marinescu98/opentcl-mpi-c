#include "T1_3.h"

void compute_generate_print(int process_id, int frq[], int size)
{
	auto start = MPI_Wtime();//Get the timepoint before the function is called

	printf("Process %d generated generated the following array: ", process_id);

	int sum = 0;

	int no;

	for (int j = 0; j < size; j++) {

		do {
			no = rand() % 1000;
		} while (frq[no] != 0);

		frq[no]++;

		printf("%d ", no);

		sum += no;
	}

	printf("\nsum = %d\n", sum);

	auto end = MPI_Wtime();//Get the timepoint after the function is called 

	printf("Runtime: %f \n", end-start);

}

void Task1_3() {

	MPI_Init(NULL, NULL);//initializare MPI

	int  number_of_process, process_id;

	MPI_Comm_size(MPI_COMM_WORLD, &number_of_process);//numarul de procese

	MPI_Comm_rank(MPI_COMM_WORLD, &process_id);//identificatorul unic al procesului

	int max_length = (rand() % (100 - number_of_process + 1)) + number_of_process;//numarul de elemente din vector

	printf("\n\n");

	int frq_array[1001] = { 0 };

	//ordinea in care se trimit: 0 -> 1 -> 2 -> ... -> 9 -> 0
	if (process_id != 0) {
		//primul proces nu poate primi deoarece lui nu i s-a trimis nimic
		MPI_Recv(&frq_array, 1000, MPI_INT, process_id - 1, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
	}

	compute_generate_print(process_id, frq_array, max_length);

	//trimitem urmatorului proces arrayul
	MPI_Send(&frq_array, 1000, MPI_INT, (process_id + 1) % number_of_process, 0, MPI_COMM_WORLD);

	if (process_id == 0) {
		//dupa ce trimite procesul cu idul 0 poate primi ceva de la procesul 9
		//astfel se asigura ca cel putin un MPI_Send este initializare inaintea tuturor MPI_Recvs pentru a preveni deadlock-ul
		MPI_Recv(&frq_array, 1000, MPI_INT, number_of_process - 1, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

	}

	MPI_Finalize();
}

