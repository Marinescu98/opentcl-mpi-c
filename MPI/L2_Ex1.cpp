#include "L2_Ex1.h"

void L2_Ex1_main()
{
	int numprocs, rank, sendcount, recvcount, source;

	float sendbuf[SIZE][SIZE] = {{1.0, 2.0, 3.0, 4.0},
						{5.0, 6.0, 7.0, 8.0},
						{9.0, 10.0, 11.0, 12.0},
						{13.0, 14.0, 15.0, 16.0}  
	};

	float recvbuf[SIZE];
	
	MPI_Init(NULL, NULL); 
	MPI_Comm_rank(MPI_COMM_WORLD, &rank); 
	MPI_Comm_size(MPI_COMM_WORLD, &numprocs); 
	
	if (numprocs == SIZE) 
	{ 
		source = 1; 
		sendcount = SIZE; 
		recvcount = SIZE; 

		//imparte vectorul si distribuie fiecarui proces o parte din el
		//parametrii: vectorul, numarul de elemente care se trimit fiecarui proces,tipul elementelor din vector,size-ul vectorului, procesul catre care se trimite,
		MPI_Scatter(sendbuf, sendcount, MPI_FLOAT, recvbuf, recvcount, MPI_FLOAT, source, MPI_COMM_WORLD); 
		printf("rank= %d  Results: %f %f %f %f\n", rank, recvbuf[0], recvbuf[1], recvbuf[2], recvbuf[3]); 
	} else
		printf("Must specify %d processors. Terminating.\n", SIZE); 
	
	MPI_Finalize();
}