#include "T1_1.h"

void search_prime(int vect[])
{
	vect[0] = vect[1] = 1;

	for (int i = 4; i <= 100; i++)
	{
		if (i != 2 && i != 3 && i != 7 && i != 5)
		{
			for (int j = 2; j < 10; j++)
			{
				if (i%j == 0)
				{
					vect[i] = 1;
				}
			}
		}
	}

}

void prime(int vector[],int all[], int process_id, int n, int number_processes)
{
	int min = n / number_processes;
	int rest = n % number_processes;

	int lower = process_id == 0 ? process_id * min + 1 : process_id * min + 1 + rest;
	int upper = (process_id + 1)*min + rest;

	printf("Process %d has the range [%d,%d].\n", process_id, lower, upper);

	for (int j = lower; j <= upper; j++) {

		if (all[j]==0)
		{
			vector[++vector[0]] = j;
		}
	}
}

void print(int v[])
{
	for (int i = 1; i <= v[0]; i++)
		printf("%d ", v[i]);
	printf("\n");
}

void Task1_1() {

	MPI_Init(NULL, NULL);//initializare MPI

	int  number_of_process, process_id;

	MPI_Comm_size(MPI_COMM_WORLD, &number_of_process);
	
	MPI_Comm_rank(MPI_COMM_WORLD, &process_id);//identificatorul unic al procesului

	int N_number = (rand() % (100 - number_of_process + 1)) + number_of_process;

	printf("\n\nN=%d\n", N_number);

	int range[101] = { 0 };
	search_prime(range);

	int prime_numbers[100];
	prime_numbers[0] = 0;

	// Receive from the lower process and send to the higher process. Take care
	// of the special case when you are the first process to prevent deadlock.
	if (process_id != 0) {
		MPI_Recv(&prime_numbers, 100, MPI_INT, process_id - 1, 0, MPI_COMM_WORLD,
			MPI_STATUS_IGNORE);
		printf("Process %d received numbers from process %d\n", process_id, process_id - 1);
	}

	prime(prime_numbers,range, process_id,N_number, number_of_process);
	print(prime_numbers);

	MPI_Send(&prime_numbers, 100, MPI_INT, (process_id + 1) % number_of_process, 0,
		MPI_COMM_WORLD);
	
	// Now process 0 can receive from the last process. This makes sure that at
	// least one MPI_Send is initialized before all MPI_Recvs (again, to prevent
	// deadlock)
	
	if (process_id == 0) {
		MPI_Recv(&prime_numbers, 100, MPI_INT, number_of_process - 1, 0, MPI_COMM_WORLD,
			MPI_STATUS_IGNORE);
		printf("Process %d received numbers from process %d\n", process_id, number_of_process - 1);
	}

	MPI_Finalize();
}

