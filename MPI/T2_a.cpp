#include "T2_a.h"

void generate_array2a(int vect[], int n)
{
	srand(9);

	for (int i = 0; i < n; i++)
	{
		vect[i] = rand() % 1000;
	}

	vect[0] = 888;
}



void print(int vect[], int n)
{
	printf("\nArray: ");
	for (int i = 1; i < n; i++)
	{
		printf("v[%d]=%d\n ",i,vect[i]);
	}
	printf("\n");

}

int result;

void T2_a() {
	
	int myid, numprocs;
	int data[MAXSIZE], x, low, high, myresult=0;

	MPI_Init(NULL, NULL);

	MPI_Comm_size(MPI_COMM_WORLD, &numprocs);
	MPI_Comm_rank(MPI_COMM_WORLD, &myid);

	if (0 == myid)
	{
		generate_array2a(data, MAXSIZE);
		print(data, MAXSIZE);
		printf("Number search: %d\n\n", data[0]);
		
	}

	MPI_Bcast(data, MAXSIZE, MPI_INT, 0, MPI_COMM_WORLD);

	result = result < 0 ? -result : result;

	/* add portion of data */
	x = MAXSIZE / numprocs;

	/* must be an integer */
	low = myid * x+1;
	high = low + x-1;

	
	for (int i = low; i < high; i++)
	{
		if (data[i] == data[0] && i>result)
		{
			if (result == 0)
				myresult = i;
			else
				myresult = -(i+result);
		}
	}

	printf("Process %d: [%d,%d]; result: %d\n", myid, low, high, myresult);

	/* compute global sum */
	
	MPI_Reduce(&myresult, &result, 1, MPI_INT, MPI_SUM, 0, MPI_COMM_WORLD);

	printf("I got %d from %d\n\n", result, myid);

	if (numprocs-1 == myid) {
		printf("!!!!!!!!!!!!The position is %d.!!!!!!!!!!!!\n", myresult);
	}

	MPI_Finalize();
}