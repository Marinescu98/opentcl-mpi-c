#include "T4.h"
	


void  T4()
{
	int world_rank, numtasks;
	int sum_final = 0, min_final = 999, max_final = 0;
	long int prod_final = 1;

	int data[] = { 22, 33, 1, 3, 4, 5, -6, 7, 9, 12, 11 };
	int n_data = 11;

	int rank1[] = { 0, 4 };//sum
	int rank2[] = { 1, 5 };//prod
	int rank3[] = { 2, 6 };//max
	int rank4[] = { 3, 7 };//min

	int sum_temp, prod_temp, min_temp, max_temp, new_rank;

	MPI_Group  orig_group, new_group;
	MPI_Comm  new_comm;

	MPI_Init(NULL, NULL);
	MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);
	MPI_Comm_size(MPI_COMM_WORLD, &numtasks);

	if (numtasks != NPROCS)
	{
		printf("Must specify MP_PROCS= %d. Terminating.\n", NPROCS);
		MPI_Finalize();
		exit(0);
	}

	// Extract the original group handle 
	MPI_Comm_group(MPI_COMM_WORLD, &orig_group);

	//construct groups
	switch (world_rank % 4)
	{
	case 0:
	{
		MPI_Group_incl(orig_group, 2, rank1, &new_group);
		break;
	}
	case 1:
	{
		MPI_Group_incl(orig_group, 2, rank2, &new_group);
		break;
	}
	case 2:
	{
		MPI_Group_incl(orig_group, 2, rank3, &new_group);
		break;
	}
	default:
	{
		MPI_Group_incl(orig_group, 2, rank4, &new_group);
		break;
	}
	}

	//Create new new communicator and then perform collective communications
	MPI_Comm_create(MPI_COMM_WORLD, new_group, &new_comm);

	MPI_Group_rank(new_group, &new_rank);

	int low = new_rank == 0 ? 0 : n_data / 2;
	int high = new_rank == 0 ? n_data / 2 : n_data;

	sum_temp = 0;
	prod_temp = 1;
	max_temp = 0;
	min_temp = 999;

	for (int i = low; i < high; i++)
	{
		switch (world_rank % 4)
		{
		case 0:
		{
			//sum
			sum_temp += data[i];
			break;
		}
		case 1:
		{
			//prod
			prod_temp *= data[i];
			break;
		}
		case 2:
		{
			//max
			if (data[i] > max_temp)
				max_temp = data[i];
			break;
		}
		default:
		{
			//min
			if (data[i] < min_temp)
				min_temp = data[i];
			break;
		}
		}
	}
	
	//printf("new_rank %d ->  maxtemp=%d low=%d high=%d\n\n", new_rank,  max_temp, low, high);

	switch (world_rank % 4)
	{
	case 0:
	{
		MPI_Reduce( &sum_temp,&sum_final, 1, MPI_INT, MPI_SUM,0, new_comm);
		break;
	}
	case 1:
	{
		MPI_Reduce(&prod_temp,&prod_final,  1, MPI_INT, MPI_PROD,0, new_comm);
		break;
	}
	case 2:
	{
		MPI_Reduce(&max_temp,&max_final,  1, MPI_INT, MPI_MAX,0, new_comm);
		break;
	}
	default:
	{
		MPI_Reduce( &min_temp,&min_final, 1, MPI_INT, MPI_MIN,0, new_comm);
		break;
	}
	}

	switch (world_rank % 4)
	{
	case 0:
	{
		if (new_rank == 0)
			printf("***rank -> %d: sum_final= %d\n", world_rank, sum_final);
		break;
	}
	case 1:
	{
		if (new_rank == 0)
			printf("***rank -> %d:  prod_final= %d\n", world_rank, prod_final);
		break;
	}
	case 2:
	{
		if (new_rank == 0)
			printf("***rank -> %d: max_final=%d\n", world_rank, max_final);
		break;
	}
	default:
	{
		if (new_rank == 0)
			printf("***rank -> %d: min_final= %d \n", world_rank, min_final);
		break;
	}
	}

	MPI_Finalize();
}