#include"T3.h"

bool compareT3(const Student& st, const Student& d)
{
	if (strcmp(st.name, d.name) == 0 && st.age == d.age)
		return true;
	return false;
}

void T3_main() {
	int result = 0;
	int myid, numprocs;
	int data[MAXSIZE], myresult = 0;

	int reduce_result = -1;
	int index = -1;

	Student students[] = { "Victor Buica", 18,
			"Maria Sirbu", 21,
			"Carla McGreen", 23,
			"Victor Valentin", 24,
			"Bogdan Cotoi", 22,
			"Mara Stefanescu", 19,
			"Bogdan Cotoi", 21,
			"Victor Buica", 18,
			"Mara Stefanescu", 19,
			"Rex McGreen", 23,
	};

	Student find = { "Mara Stefanescu",19 };

	MPI_Init(NULL, NULL);

	MPI_Comm_size(MPI_COMM_WORLD, &numprocs);
	MPI_Comm_rank(MPI_COMM_WORLD, &myid);

	MPI_Datatype studenttype;
	MPI_Datatype types[] = { MPI_CHAR, MPI_INT };

	int lengths[] = { 50, 1 };
	const MPI_Aint displacements[] = { sizeof(int) + sizeof(char) ,0 };

	MPI_Type_create_struct(2, lengths, displacements, types, &studenttype);
	MPI_Type_commit(&studenttype);

	MPI_Bcast(students, MAXSIZE, studenttype, 0, MPI_COMM_WORLD);

	/* add portion of data */
	int x = MAXSIZE / numprocs;

	/* must be an integer */
	int low = myid * x;
	int high = low + x;

	for (int i = low; i < high; i++)
	{
		if (compareT3(students[i], find))
		{
			index = myid;
		}
	}

	MPI_Reduce(&reduce_result, &index, 1, MPI_INT, MPI_MAX, 0, MPI_COMM_WORLD);


		if (index != -1)
		{
			printf("Student search for: %s, %d years old, process = %d\n", find.name, find.age, myid);
		}

	MPI_Finalize();
}