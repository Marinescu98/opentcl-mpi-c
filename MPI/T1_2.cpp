#include "T1_2.h"

void generate_array(int vect[])
{
	for (int i = 2; i < vect[0]; i++)
	{
		vect[i] = rand()%100;
	}

}

int search(int vector[], int process_id, int n, int number_processes)
{
	int min = n / number_processes;
	int rest = n % number_processes;

	int lower = process_id == 0 ? 2 : process_id * min + 2 + rest;
	int upper = (process_id + 1)*min + rest+1;

	printf("Process %d has the range [%d,%d].\n", process_id, lower-2, upper-2);

	for (int j = lower; j <= upper; j++) {

		if (vector[j] == vector[1])
		{
			return j-2;
		}
	}

	return -1;
}

void print2(int v[])
{
	for (int i = 2; i < v[0]; i++)
		printf("%d ", v[i]);
	printf("\n");
}

void Task1_2() {

	MPI_Init(NULL, NULL);//initializare MPI

	int  number_of_process, process_id;

	MPI_Comm_size(MPI_COMM_WORLD, &number_of_process);//numarul de procese

	MPI_Comm_rank(MPI_COMM_WORLD, &process_id);//identificatorul unic al procesului

	int N_number = (rand() % (100 - number_of_process + 1)) + number_of_process;//numarul de lemente din vector
	int array[103];//vectorul in care se face cautarea
	
	array[0] = N_number;//prima pozitie reprezinta numarul de elemente din vector
	array[1] = 5;//rand() % 100;//a doua cuprinde numarul de cautat
	generate_array(array);//generam numerele
	
	printf("\n\nN=%d\n", N_number);
	printf("Number_searched=%d\n", array[1]);
	print2(array);

	//ordinea in care se trimit: 0 -> 1 -> 2 -> ... -> 9 -> 0
	if (process_id != 0) {
		//primul proces nu poate primi deoarece lui nu i s-a trimis nimic
		MPI_Recv(&array, N_number, MPI_INT, process_id - 1, 0, MPI_COMM_WORLD,MPI_STATUS_IGNORE);
		printf("Process %d received array from process %d\n", process_id, process_id - 1);
	}

	int retu = search(array, process_id, N_number, number_of_process);//se face cautarea

	if ( retu != -1)
		printf("!!!!!!!!!!!!!!!!!!!!!!!!!The number was found here at %d!!!!!!!!!!!!!!!!!!!!!!!!!!\n",retu);
	else
		printf("The number was not found here!\n");

	//trimitem urmatorului proces arrayul
	MPI_Send(&array, N_number, MPI_INT, (process_id + 1) % number_of_process, 0,MPI_COMM_WORLD);

	if (process_id == 0) {
		//dupa ce trimite procesul cu idul 0 poate primi ceva de la procesul 9
		//astfel se asigura ca cel putin un MPI_Send este initializare inaintea tuturor MPI_Recvs pentru a preveni deadlock-ul
		MPI_Recv(&array, N_number, MPI_INT, number_of_process - 1, 0, MPI_COMM_WORLD,MPI_STATUS_IGNORE);
		printf("Process %d received array from process %d\n", process_id, number_of_process - 1);
	}

	MPI_Finalize();
}

