#include "Criptare.h"
#include <iostream>

ifstream fin_cipher("Input_Criptare.txt");
ifstream fin_dec("Input_Decriptare.txt");


char readText[MAXSIZE], cipherText[MAXSIZE], originalText[MAXSIZE];
string key;
char keyword[MAXSIZE];

string generateKey(string str, string key)
{
	int x = str.size();

	for (int i = 0; ; i++)
	{
		if (x == i)
			i = 0;
		if (key.size() == str.size())
			break;
		key.push_back(key[i]);
	}
	return key;
}

void cipher_text(int low, int high)
{

	for (int i = low; i <= high; i++)
	{
		// converting in range 0-25 
		char x = (readText[i] + key[i]) % 26;

		// convert into alphabets(ASCII) 
		x += 'A';

		cipherText[i] = x;
	}

}

void decrypted_text(int low, int high)
{
	for (int i = low; i <= high; i++)
	{
		// converting in range 0-25 
		char x = (readText[i] - key[i] + 26) % 26;

		// convert into alphabets(ASCII) 
		x += 'A';
		
		originalText[i] = x;
	}
}

void Criptare() {
	//declaratii
	int myid, numprocs, low, high;
	char x;
	Status status = Decrypted; 
//	Status status = Encrypted;

	//citirea datelor din fisier in functie de operatie: criptare sau descriptare
	if (status == Encrypted)
	{
		fin_cipher >> readText;
		fin_cipher >> keyword;
	}
	else {
		fin_dec >> readText;
		fin_dec >> keyword;
	}

	key = generateKey(readText, keyword);


	MPI_Init(NULL, NULL);
	MPI_Comm_size(MPI_COMM_WORLD, &numprocs);
	MPI_Comm_rank(MPI_COMM_WORLD, &myid);

	//trimitem datele.
	//ordinea in care se trimit: 0 -> 1 -> 2 -> ... -> 9 -> 0
	//primul proces nu poate primi deoarece lui nu i s-a trimis nimic
	if (myid != 0)
	{
		if (status == Encrypted)
		{
			MPI_Recv(&cipherText, strlen(readText), MPI_CHAR, myid - 1, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
		}
		else {
			MPI_Recv(&originalText, strlen(readText), MPI_CHAR, myid - 1, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
		}
	}

	//calculam limitele
	x = (strlen(readText) - 1) / numprocs;
	low = myid * x;
	high = myid == numprocs - 1 ? strlen(readText) : low + x - 1;

	//efectuam operatia de criptare sau decriptare
	if (status == Encrypted)
	{
		cipher_text(low, high);
	}
	else {
		
		decrypted_text(low, high);
	}

	//trimitem urmatorului proces rezultatul operatiei de criptare sau decriptare
	if (status == Encrypted) {
		MPI_Send(&cipherText, strlen(readText), MPI_CHAR, (myid + 1) % numprocs, 0, MPI_COMM_WORLD);
	}
	else
	{
		MPI_Send(&originalText, strlen(readText), MPI_CHAR, (myid + 1) % numprocs, 0, MPI_COMM_WORLD);
	}

	//afisam rezultatul
	if (myid == 0) {
		if (status == Encrypted) {
			MPI_Recv(&cipherText, strlen(readText), MPI_CHAR, numprocs - 1, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
		}
		else
		{
			MPI_Recv(&originalText, strlen(readText), MPI_CHAR, numprocs - 1, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
		}
		printf("Text: %s\n", readText);
		printf("Keyword: %s\n", keyword);
		cout << "key: " << key << endl;

		if (status == Encrypted)
		{
			cipherText[strlen(readText)] = '\0';
			cout << "Encrypted text: " << cipherText << endl;
		}
		else {
			originalText[strlen(readText)] = '\0';
			cout << "Original text: " << originalText << endl;
		}
	}

	MPI_Finalize();
}