#include "L2_Ex3.h"

void generate_array(int vect[], int n)
{
	for (int i = 0; i < n; i++)
	{
		vect[i] = rand() % 100;
	}

}

void L2_Ex3_main() {
	int myid, numprocs; 
	int data[MAXSIZE], i, x, low, high, myresult = 0, result; 
	
	MPI_Init(NULL, NULL); 
	
	MPI_Comm_size(MPI_COMM_WORLD, &numprocs); 
	MPI_Comm_rank(MPI_COMM_WORLD, &myid);

	if(0==myid)
		generate_array(data, MAXSIZE);
	
	MPI_Bcast(data, MAXSIZE, MPI_INT, 0, MPI_COMM_WORLD);
	
	/* add portion of data */
	x = MAXSIZE / numprocs;   
	
	/* must be an integer */
	low = myid * x; 
	high = low + x; 
	
	for (i = low; i < high; i++) 
	{ 
		myresult += data[i]; 
	}
	
	printf("I got %d from %d\n", myresult, myid);
	
	/* compute global sum */
	
	MPI_Reduce(&myresult, &result, 1, MPI_INT, MPI_SUM, 0, MPI_COMM_WORLD); 
	
	if (0 == myid) { 
		printf("The sum is %d.\n", result); 
	}
	
	MPI_Finalize();
}