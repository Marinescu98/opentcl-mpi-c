#pragma once

#include "mpi.h"
#include <math.h>
#include <stdio.h>
#include <stdlib.h>

void generate_array(int vect[]);
int search(int vector[], int no_s, int process_id, int n, int number_processes);
void Task1_2();
void print2(int v[]);