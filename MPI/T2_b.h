#pragma once
#include "mpi.h"
#include <stdio.h>
#include <math.h>
#include <stdlib.h>

#define MAXPROC 10 /* Max number of procsses */ 
#define MAXSIZE 100

void generate_array2b(int vect[], int n);

void T2_b();

void print_B(int vect[], int n);
